from unittest import TestCase
import unittest

def Sumfunct(x, y):
    return x + y

class TestSum(TestCase):
    def test_sum(self):
        # first store the expected result in a variable
        result = Sumfunct(3, 4)
        # check if the result is equal to expected result
        # here the result should be equal to 7.
        self.assertEqual(7, result)
        # self.assertEqual(9, result)
if __name__ == "__main__":
    unittest.main()