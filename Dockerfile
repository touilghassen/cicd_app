FROM python:3.9

# ENV GROUP_ID=1000 \
#     USER_ID=1000

WORKDIR /var/code/

ADD . /var/code/

RUN pip install -r requirements.txt

EXPOSE 8000

ENTRYPOINT ["python3" , "/var/code/main.py"]